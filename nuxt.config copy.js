import pkg from './package'

const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: "Helen said Yes!",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      // icon
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // Bulma CSS
      { rel: 'stylesheet', href: 'https://unpkg.com/bulma@0.9.4/css/bulma.min.css' },
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.4/dist/css/bulma-carousel.min.css' },
      // Google fonts
      { rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', integrity: 'sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN', crossorigin: 'anonymous' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: true }, 
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Dancing+Script&family=Great+Vibes&family=Lato:wght@100;300&family=Open+Sans:ital,wght@1,300&display=swap' },
      { rel: 'stylesheet', href: 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css' },
    ],
    script: [
      { src: 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js' },
      { src: 'https://kit.fontawesome.com/2828f7885a.js', integrity: 'sha384-WAsFbnLEQcpCk8lM1UTWesAf5rGTCvb2Y+8LvyjAAcxK1c3s5c0L+SYOgxvc6PWG', crossorigin: 'anonymous' },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },

  /*
  ** Customize the base url
  */
  router: {
    base
  },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}