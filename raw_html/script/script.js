bulmaCarousel.attach("#slider", {
    slidesToScroll: 1,
    slidesToShow: 3,
    infinite: true
});


AOS.init({
    easing: "ease-out",
    duration: 800,
});
